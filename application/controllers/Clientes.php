<?php
 class Clientes extends CI_Controller{
  public function __construct(){
    parent:: __construct();
    $this->load->model("cliente");
    if ($this->session->userdata('c0nectadoUTC')) {
      //SI ESTA CONECTADO
      // code...
    } else {
      redirect('seguridades/formularioLogin');
    }
  }
  public function index(){
    $data["listadoClientes"]=$this->cliente->consultarTodos();
    $this->load->view("header");
    $this->load->view("clientes/index",$data);
    $this->load->view("footer");
  }
  public function nuevo(){
        $data["listadoClientes"]=$this->cliente->consultarTodos();
        $this->load->view("header");
        $this->load->view("clientes/nuevo",$data);
        $this->load->view("footer");
      }
      public function editar($id_cli){
        $data['cliente']=$this->cliente->consultarPorId($id_cli);
        $this->load->view("header");
        $this->load->view("clientes/editar",$data);
        $this->load->view("footer");
      }

  public function guardarCliente(){
    $datosNuevoCliente=array(
          "apellido_cli"=>$this->input->post("apellido_cli"),
          "nombre_cli"=>$this->input->post("nombre_cli"),
          "telefono_cli"=>$this->input->post("telefono_cli"),
          "casa_numero_cli"=>$this->input->post("casa_numero_cli"),
          "email_cli"=>$this->input->post("email_cli")
        );
      if ($this->cliente->insertar($datosNuevoCliente)) {
          // echo "Insercion Exitosa";
          // code...
          $this->session->set_flashdata('confirmacion','Cliente insertado exitosamente');
          } else {
            $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
          }
          redirect("clientes/index");
  }

  function procesarEliminacion($id_cli){
    if($this->cliente->eliminar($id_cli)){
      $this->session->set_flashdata('confirmacion','Cliente insertado exitosamente');
          } else {
            $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
          }
          redirect("clientes/index");
  }
  public function procesarActualizacion(){
    $id_cli = $this->input->post("id_cli");
    $datosClienteEditado=array(
      "apellido_cli"=>$this->input->post("apellido_cli"),
      "nombre_cli"=>$this->input->post("nombre_cli"),
      "telefono_cli"=>$this->input->post("telefono_cli"),
      "casa_numero_cli"=>$this->input->post("casa_numero_cli"),
      "email_cli"=>$this->input->post("email_cli")

    );

    if ($this->cliente->actualizar($id_cli,$datosClienteEditado)) {
            //echo "INSERCION EXITOSA";
            $this->session->set_flashdata('confirmacion','CLIENTE EDITADO EXITOSAMENTE');
          } else {
            $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
          }
          redirect("clientes/index");
        }




      }

 ?>
