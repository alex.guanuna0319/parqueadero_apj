<?php
class Empleados extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("empleado");
        if ($this->session->userdata('c0nectadoUTC')) {
          //SI ESTA CONECTADO
          // code...
        } else {
          redirect('seguridades/formularioLogin');
        }

      }
        //validando si alguien esta conectado ESTO ES LO PRIMORDIAL
    }

  public function index(){
    $this->load->view("header");
    $this->load->view("empleados/index");
    $this->load->view("footer");
  }
  //funcion para frenderizar la vista listado.php
  public function listado(){
    $data["listadoEmpleados"]=$this->empleado->obtenerTodos();
    $this->load->view("empleados/listado",$data);

  }
  public function editar($id_em){
    $data['empleado']=$this->empleado->consultarPorId($id_em);
    $this->load->view("header");
    $this->load->view("empleados/editar",$data);
    $this->load->view("footer");
  }
  ///Inserciion Asincrona
  public function insertarEmpleado(){
    $data=array(
      "nombre_em"=>$this->input->post("nombre_em"),
      "apellido_em"=>$this->input->post("apellido_em"),
      "email_em"=>$this->input->post("email_em"),
      "telefono_em"=>$this->input->post("telefono_em"),
      "cargo_em"=>$this->input->post("cargo_em"),


    );
    if($this->empleado->insertarEmpleado($data)){
      echo json_encode(array("respuesta"=>"ok"));
      redirect("empleados/index");
    }else{
      echo json_encode(array("respuesta"=>"error"));

    }
  }
  public function procesarEliminacion($id_em){

    $data['empleado']=$this->empleado->obtenerPorId($id_em);
        if($this->empleado->eliminar($id_em)){
            redirect("empleados/index");
  }
      else{
            echo "ERROR AL ELIMINAR";
          }
    
  }
  public function procesarActualizacion(){
    $id_em = $this->input->post("id_em");
    $datosEmpleadoEditado=array(
      "nombre_em"=>$this->input->post("nombre_em"),
      "apellido_em"=>$this->input->post("apellido_em"),
      "email_em"=>$this->input->post("email_em"),
      "telefono_em"=>$this->input->post("telefono_em"),
      "cargo_em"=>$this->input->post("cargo_em")
    );
    
    if ($this->empleado->actualizar($id_em,$datosEmpleadoEditado)) {
    
                //Pongan esto para que al momento de editar salga el mensaje flash
                $this->session->set_flashdata('confirmacion','CLIENTE EDITADO EXITOSAMENTE');
              } else {
                $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
              }
              redirect("empleados/index");
              }

  }//cierre de la clase
