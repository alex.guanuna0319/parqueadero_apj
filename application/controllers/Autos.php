<?php
 class Autos extends CI_Controller{
  public function __construct(){
    parent:: __construct();
    $this->load->model("auto");
    $this->load->model("marca");
    if ($this->session->userdata('c0nectadoUTC')) {
      //SI ESTA CONECTADO
      // code...
    } else {
      redirect('seguridades/formularioLogin');
    }

  }
  }
  public function index(){
    $data["listadoAutos"]=$this->auto->consultarTodos();
    $this->load->view("header");
    $this->load->view("autos/index",$data);
    $this->load->view("footer");
  }
  public function nuevo(){
        $data["listadoAutos"]=$this->auto->consultarTodos();
        $this->load->view("header");
        $this->load->view("autos/nuevo",$data);
        $this->load->view("footer");
      }
      public function editar($id_at){
        $data['auto']=$this->auto->consultarPorId($id_at);
        $this->load->view("header");
        $this->load->view("autos/editar",$data);
        $this->load->view("footer");
      }
  public function guardarAuto(){
    $datosNuevoAuto=array(
      "modelo_at"=>$this->input->post("modelo_at"),
      "marca_at"=>$this->input->post("marca_at"),
      "ano_at"=>$this->input->post("ano_at"),
      "color_at"=>$this->input->post("color_at"),
      "placa_at"=>$this->input->post("placa_at")

        );
        $this->load->library("upload");//carga de la libreria de subida de archivos
                  $nombreTemporal="foto_at_".time()."_".rand(1,5000);
                  $config["file_name"]=$nombreTemporal;
                  $config["upload_path"]=APPPATH.'../uploads/clientes/';
                  $config["allowed_types"]="jpeg|jpg|png";
                  $config["max_size"]=2*1024;//2mb
                  $this->upload->initialize($config);
                  //codigo para subir el archivo y guradar el nombre en la BBD
                  if ($this->upload->do_upload("foto_at")) {
                    $dataSubida=$this->upload->data();
                    $datosNuevoCliente["foto_at"]=$dataSubida["file_name"];
                  }

                  if ($this->auto->insertar($datosNuevoAuto)) {
                      $this->session->set_flashdata("confirmacion","AUTO INSERTADO EXITOSAMENTE.");
                  } else {
                    $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
                  }
                  redirect("autos/index");
            }






  public function procesarActualizacion(){
          $id_at=$this->input->post("id_at");
          $datosAutoEditado=array(
            "modelo_at"=>$this->input->post("modelo_at"),
            "marca_at"=>$this->input->post("marca_at"),
            "ano_at"=>$this->input->post("ano_at"),
            "color_at"=>$this->input->post("color_at"),
            "placa_at"=>$this->input->post("placa_at")
              );
              //logica de negocio necesarioa para subir la fotografia del cliente

      $this->load->library('upload');//carga de la libreria de subida de archivos
      $nombreTemporal='foto_at_'.time().'_'.rand(1,500);//generar nombre aleatorio
      $config['file_name']=$nombreTemporal; //te,poral
      $config['upload_path']=APPPATH.'../uploads/autos/'; //ruta de la subida de los archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de datos permitidos
      $config['max_size']=4*1024;//configurar el peso maximo de los archivos
      $this->upload->initialize($config);//inicializar la configuracion
      //instruccion para que se valide la subida del archivos
      if($this->upload->do_upload('foto_at')){
        //FUNCION PARA QUE SE ME ACTUALICE LA FOTO CUANDO PONGO UNA NUEVA FOTO
      $query =$this->cliente->obtenerPorId($id_at);
      unlink(APPPATH.'../uploads/autos/'.$query->foto_at);


      $dataSubida=$this->upload->data();
      $datosAutoEditado['foto_at']=$dataSubida['file_name'];

      }

      if ($this->auto->actualizar($id_at,$datosAutoEditado)) {

                  //Pongan esto para que al momento de editar salga el mensaje flash
                  $this->session->set_flashdata('confirmacion','AUTO EDITADO EXITOSAMENTE');
                } else {
                  $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
                }
                redirect("autos/index");
                }
            }//cierre de controlador




 ?>
