<?php

class Auto extends CI_Model{
public function __construct(){
    parent ::__construct();
  }
  public function insertar($datos){
    return $this->db -> insert("auto",$datos);
  }
  public function actualizar($id_at,$datos){
      $this->db->where("id_at",$id_at);
      return $this->db->update("auto",$datos);
    }
  public function consultarTodos(){
    $listadoAutos=$this->db->get('auto');
    if ($listadoAutos->num_rows()>0){
      return $listadoAutos;
    }else{
      return false;
    }
  }
  public function eliminar($id_at){
        $this->db->where("id_at",$id_at);
        return $this->db->delete("auto");
      }
      public function consultarPorId($id_at){
        $this->db->where('id_at',$id_at);
        $auto=$this->db->get("auto");
        if ($auto->num_rows()>0){
          //cuando hay clientes
          return $auto->row();
        }else{
          //cuando no hay clientes
          return false;
        }
       }
       public function obtenerPorId($id_at){
        $this->db->where('id_at',$id_at);
        $query= $this->db->get('auto');
        if ($query->num_rows()>0) {
          // code...
          return $query->row();
        } else {
          return false;
        }
      }
}
 ?>
