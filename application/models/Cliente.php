<?php

class Cliente extends CI_Model{
public function __construct(){
    parent ::__construct();
  }
  public function insertar($datos){
    return $this->db -> insert("cliente",$datos);
  }
  public function actualizar($id_cli,$datos){
      $this->db->where("id_cli",$id_cli);
      return $this->db->update("cliente",$datos);
    }
  public function consultarTodos(){
    $listadoClientes=$this->db->get('cliente');
    if ($listadoClientes->num_rows()>0){
      return $listadoClientes;
    }else{
      return false;
    }
  }
  public function eliminar($id_cli){
        $this->db->where("id_cli",$id_cli);
        return $this->db->delete("cliente");
      }
      public function consultarPorId($id_cli){
        $this->db->where('id_cli',$id_cli);
        $cliente=$this->db->get("cliente");
        if ($cliente->num_rows()>0){
          //cuando hay clientes
          return $cliente->row();
        }else{
          //cuando no hay clientes
          return false;
        }
       }
       public function obtenerPorId($id_cli){
        $this->db->where('id_cli',$id_cli);
        $query= $this->db->get('cliente');
        if ($query->num_rows()>0) {
          // code...
          return $query->row();
        } else {
          return false;
        }
      }
}
 ?>
