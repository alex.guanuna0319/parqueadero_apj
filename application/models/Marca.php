<?php
    //constructor
    class Marca extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para consultar Pais
        public function consultarTodos(){
          //variable
            $listadoMarcas=$this->db->get('marca');
            if ($listadoMarcas->num_rows()>0) {
                // Cuando si hay paises registrados
                return $listadoMarcas;
            } else {
                //cuando no hay paises
                return false;
            }
        }
    }//cierre llave


 ?>
