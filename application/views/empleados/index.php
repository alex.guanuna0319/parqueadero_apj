<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<br>
<h1 class="text-center">GESTION DE EMPLEADOS</h1>
<div class="row">
  <div class="col-md-12" id="contenedor-listado-empleados">
    <i class="fa fa-spin  fa-lg fa-spinner"></i>
    CONSULTANDO DATOS
  </div>

</div>
<!-- Modal -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modalNuevoEmpleado">Agregar Empleado</button>

<!-- Modal -->
<div id="modalNuevoEmpleado" style="z-index:9999 !important;" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title"><i class="fa fa-users"></i>Nuevo Empleado</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
        <form class="" action="<?php echo site_url('empleados/insertarEmpleado');?>" method="post" id="frm_nuevo_empleado" enctype="multipart/form-data">


          <label for="">APELLIDO:</label>
<br>
<input type="text" name="apellido_em" id="apellido_em" placeholder="">
<br>
<label for="">NOMBRE:</label>
<br>
<input type="text" name="nombre_em" id="nombre_em" placeholder="">
<br>
<label for="">EMAIL:</label>
<br>
<input type="email" name="email_em" id="email_em" placeholder="">
<br><label for="">TELEFONO:</label>
<br>
<input type="number" name="telefono_em" id="telefono_em" placeholder="">

<input type="password" name="password_confirmada" id="password_confirmada" class="form-control"placeholder=""><br>
<label for="">CARGO:</label>
<br>
<select name="cargo_em" id="cargo_em">
<option value="">SELECCIONE UNA OPCION:</option>
<option value="GUARDIA">GUARDIA</option>
<option value="PORTERO">PORTERO</option>
<option value="CUIDADOR">CUIDADOR</option>
</select><br><br><br>
<button type="submit" name="button" class="btn btn-success"> <i class="fa fa-save"></i> GUARDAR</button>
<button type="reset" value="Reset" class="btn btn-warning">Reset</button>

        </form>
      </div>
      <div class="modal-footer">
      <button type="submit" name="button" class="btn btn-danger"> <i class="fa fa-save"></i> SALIR</button>

    </div>
    </div>

  </div>
</div>

<button type="button" name="button" class="btn btn-primary"onclick="cargarListadoEmpleados();">ACTUALIZAR</button>


<script type="text/javascript">
  function cargarListadoEmpleados(){
      $("#contenedor-listado-empleados").html('<i class="fa fa-spin  fa-lg fa-spinner"></i>')
    $("#contenedor-listado-empleados").load("<?php echo site_url(); ?>/empleados/listado");
  }
  cargarListadoEmpleados();
  $("#frm_nuevo_empleado").validate({
    rules:{
        apellido_em:{
          letras:true,
          required:true
        },
        nombre_em:{
          letras:true,
          required:true
        },
        email_em:{
          email:true,
          required:true
        }

      },
      messages:{
        apellido_em:{
          required:"Porfavor ingrese su apellido",
          letras:"Porfavor no ingrese numeros",
        },
        nombre_em:{
          required:"Porfavor ingrese su nombre",
          letras:"Porfavor no ingrese numeros",
        },

        email_em:{
          required:"Porfavor ingrese su email",
          email:"Email no valido utiliza un @ en el email porfavor"
        }


      },
      submitHandler:function(form){
         $.ajax({
           url:$(form).post("action"),
           type:"post",
           data:$(form).serialize(),
           success:function(data){
             alert(data);
           }
         });
       }

  });
</script>

<script type="text/javascript">
    function confirmarEliminacion(id_em){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el cliente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/empleados/procesarEliminacion/"+id_em;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
