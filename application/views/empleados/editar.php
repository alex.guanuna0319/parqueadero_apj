<br>
<form  action="<?php echo site_url(); ?>/empleados/procesarActualizacion"method="post" id="frm_editar_empleado" >
<input type="hidden" name="id_em"  id="id_em" value="<?php echo $empleado->id_em; ?>">
<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-md-10" >
      <center>  
<br>
<input type="hidden" name="fecha_actualizacion_em" id="fecha_actualizacion_em" value="<?php echo $empleado->fecha_actualizacion_em; ?>" placeholder=""min="01/01/2022" max="31/12/2022" disable>
<br>   
    <label for"">APELLIDO:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $empleado->apellido_em; ?>"name="apellido_em" id= "apellido_em" placeholder="Ingrese el apellido">
    <br>
    <label for"">NOMBRE:</label>
    <br>
    <input type="text" class="form-control"value="<?php echo $empleado->nombre_em; ?>" name="nombre_em" id= "nombre_em" placeholder="Ingrese su nombre">
    <br>
    <label for"">EMAIL:</label>
  <br>
  <input type="text" class="form-control" value="<?php echo $empleado->email_em; ?>"name="email_em" id= "email_em" placeholder="Ingrese su email">
  <br>
    <label for"">TELEFONO:</label>
   <br>
   <input type="text" class="form-control" value="<?php echo $empleado->telefono_em; ?>" name="telefono_em" id= "telefono_em" placeholder="Ingrese su numero de telefono">
   <br>
<br>
<label for="">CARGO:</label>
<br>
<select name="cargo_em" id="cargo_em" value="<?php echo $empleado->cargo_em; ?>">
<option value="">SELECCIONE UNA OPCION:</option>
<option value="GUARDIA">GUARDIA</option>
<option value="PORTERO">PORTERO</option>
<option value="CUIDADOR">CUIDADOR</option>
</select>

</center>

<div class="col-md-12 mt-4 mb-4 text-center">
  <button type="submit" name="button"  class="btn btn-primary">🐉卍ACTUALIZAR🐉卍</a></button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
  <a href="<?php echo site_url(); ?>/empleados/index" class="btn btn-warning">
     CANCELAR
   </a>

</div>
</div>
<script type="text/javascript">

  $("#frm_editar_empleado").validate({
    rules:{
        apellido_em:{
          letras:true,
          required:true
        },
        nombre_em:{
          letras:true,
          required:true
        },
        email_em:{
          email:true,
          required:true
        }

      },
      messages:{
        apellido_em:{
          required:"Porfavor ingrese su apellido",
          letras:"Porfavor no ingrese numeros",
        },
        nombre_em:{
          required:"Porfavor ingrese su nombre",
          letras:"Porfavor no ingrese numeros",
        },

        email_em:{
          required:"Porfavor ingrese su email",
          email:"Email no valido utiliza un @ en el email porfavor"
        }


      }

  });
</script>

</form>
