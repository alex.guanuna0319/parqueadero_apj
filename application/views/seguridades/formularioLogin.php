<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<div style="padding-top:10%">
<form class="" action="<?php echo site_url();?>/seguridades/validarAcceso" method= "post">
<!-- Section: Design Block -->
<section class="">
  <!-- Jumbotron -->
  <div class="px-4 py-5 px-md-5 text-center text-lg-start" style="background-color: hsl(163, 29%, 84%)">
    <div class="container">
      <div class="row gx-lg-5 align-items-center">
        <div class="col-lg-6 mb-5 mb-lg-0">
          <h1 class="my-5 display-3 fw-bold ls-tight">
            PARKAR  <br />
            <span class="text-primary">PARQUEADERO LATACUNGA</span>
          </h1>
        </div>

        <div class="col-lg-6 mb-5 mb-lg-0">
          <div class="card">
            <div class="card-body py-5 px-md-5">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->

                <!-- Email input -->
                <div class="form-outline mb-4">
                  <input type="email" name="email_usu" id="email_usu" value="" placeholder="Ingrese su email" class="form-control" />
                  <label class="form-label" for="form3Example3">Correo Electronico</label>
                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                <input type="password" name="password_usu" id="password_usu" value="" placeholder="Ingrese su contraseña" class="form-control"required>
                  <label class="form-label" for="form3Example4">Contraseña</label>
                </div>

                <!-- Checkbox -->


                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block mb-4">
                  Ingresar
                </button>

                <!-- Register buttons -->

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Jumbotron -->
</section>
<?php if ($this->session->flashdata("error")):?>
<script type="text/javascript">
alert("<?php echo $this->session->flashdata("error");?>");
</script>
<?php endif;?>

</form>

</div>
<!-- Section: Design Block -->
